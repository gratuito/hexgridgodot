# Hex Grid Utils For Godot

## Description
This library contains a set of utilities focused on hexagonal grids. Currently, it only provides two static functions that give a snap position on a hex grid given a position (e.g., mouse position).

Flat top hex grid:
![Flat top](Docs/hex_grid_flat_top.gif)
Note: Debug visuals aren't included on this library.

Pointy top hex grid:
![Pointy top](Docs/hex_grid_pointy_top.gif)
Note: Debug visuals aren't included on this library.

## Usage
Just download all the files, then drag and drop them into your project directory. We recommend adding the following classes to the "Autoload" scripts:
- HexGridUtils (Class with static methods)

![Autoload](Docs/godot_add_to_autoload.png)

## Authors
This library was developed by Tacure.

## License
This library is distributed under the MIT license. Feel free to use it on any project.
